
var handleClickMapObject = function(event){
  handleNotifications(event);
  if (event.mapObject.groupId === undefined && map.dataProvider.map!=="continentsLow"){
    if (amChartModel.clickOutsideAreas && amChartModel.clickOutsideAreas.notification){
      var config = Object.assign({}, amChartModel.clickOutsideAreas.notyConfig);
      config.text = amChartModel.clickOutsideAreas.notification;
      getButtons(config);
      var noty = new Noty(config).show();
    }
  }
}

var handleNotifications = function(event){
  if (!event.mapObject.notification)return;
  var noty;
  var config = globalNotyConfig || {};
  config.text=event.mapObject.notification;

  if (event.mapObject.notyConfig){
    for (var attrname in event.mapObject.notyConfig) {
      if (attrname === 'buttons')continue;
      config[attrname] = event.mapObject.notyConfig[attrname];
    }
    getButtons(config);
  }
  noty = new Noty(config).show();
}

function getButtons(config){
  if (config.buttons){
    var tmpButtons = [];
    config.buttons.forEach(function(button){
      var btn = Noty.button(button.title, button.class, function(){
        if (button.url){
          document.location.href = button.url;
        }else{
          noty.close()
        }
      });
      tmpButtons.push(btn);
    });
    config.buttons = tmpButtons;
  }
}

var map = AmCharts.makeChart("mapdiv", amChartModel);
map.addListener('clickMapObject', handleClickMapObject);
