var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content')
};
var defaultTargetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

exports.getNotificationConfig = function(model, notification){
  return setNotificationConfig(model,notification);
}

exports.getAmChartModel = function(content){
    var amChartModel = getAmChartModelBase();
    setDataProviders(amChartModel, content);
    return amChartModel;
};

function setDataProviders(amChartModel, content){
  amChartModel.worldDataProvider = getWorldDataProvider(content);
  amChartModel.continentsDataProvider = getContinentsDataProvider(amChartModel.worldDataProvider);
  amChartModel.dataProvider = amChartModel.continentsDataProvider;
}

function getContinentsDataProvider(worldDataProvider){
  var continentsDataProvider = {
    "map": "continentsLow",
    "areas": [ {
      "id": "africa",
      "linkToObject": worldDataProvider,
      "color": "#605675",
      "passZoomValuesToTarget": true
    }, {
      "id": "asia",
      "linkToObject": worldDataProvider,
      "color": "#a791b4",
      "passZoomValuesToTarget": true
    }, {
      "id": "australia",
      "linkToObject": worldDataProvider,
      "color": "#7f7891",
      "passZoomValuesToTarget": true
    }, {
      "id": "europe",
      "linkToObject": worldDataProvider,
      "color": "#9186a2",
      "passZoomValuesToTarget": true
    }, {
      "id": "north_america",
      "linkToObject": worldDataProvider,
      "color": "#868191",
      "passZoomValuesToTarget": true
    }, {
      "id": "south_america",
      "linkToObject": worldDataProvider,
      "color": "#8f7ea9",
      "passZoomValuesToTarget": true
    } ]
  };
  return continentsDataProvider;
}

function getWorldDataProvider(content){
  var globalConfig = content.data.config | '';
  var worldDataProvider = {
    map: "worldLow",
    images: [],
    areas:[],
    getAreasFromMap: true
  };
  
  /*Set areas*/
  var areas = forceArray(content.data.areas);
  forceArray(areas).forEach(function(area, areaIndex, areaArray){
    if (!(area && area.id))return;
    forceArray(area.id).forEach(function(idElement, idIndex, idArray){
      var areaModel = {};
      areaModel.id = idElement;
      areaModel.groupId = areaIndex;
      setConfig(areaModel, area.config, globalConfig);
      worldDataProvider.areas.push(areaModel);
    });
  });

  /*Set images*/
  var images = forceArray(content.data.images);
  forceArray(images).forEach(function(image){
    if (!image)return;
    var imageModel = {
      svgPath: defaultTargetSVG,
      zoomLevel: 3,
      scale: 0.5
    };
    if (image.title){
      imageModel.title = image.title;
    }
    if (image.geopoint){
      imageModel.latitude = image.geopoint.split(',')[0];
      imageModel.longitude = image.geopoint.split(',')[1];
    }

    setConfig(imageModel, image.config, globalConfig);
    worldDataProvider.images.push(imageModel);
  });
  return worldDataProvider;
}

function setConfig(model, config, globalConfig){
  if (!config || !config._selected)return model;
  var selectedConfigs = forceArray(config._selected);
  selectedConfigs.forEach(function (selectedConfig){
    if (selectedConfig === 'label'){
      setLabelConfig(model, config.label, globalConfig.labels);
    }else if (selectedConfig === 'url'){
      setUrlConfig(model, config.url);
    }else if (selectedConfig === 'color'){
      setColorConfig(model, config.color);
    }else if (selectedConfig === 'notification'){
      setNotificationConfig(model, config.notification);
    }
  });
  return model;
}

function setLabelConfig(model, label, globalLabelsConfig){
    model.label = label.label || '';
    model.title = label.title || '';
    model.zoomX = 0;
    model.zoomY = 0;

    if (label.description){
      model.description = libs.portal.processHtml({value:label.description});
    }
    if (label.balloonText){
      model.balloonText = libs.portal.processHtml({value:label.balloonText});
    }
    model.labelShiftX = label.labelShiftX || getGlobalConfig(globalLabelsConfig, 'labelShiftX', 0);
    model.labelShiftY = label.labelShiftY || getGlobalConfig(globalLabelsConfig, 'labelShiftY', 0);
    model.labelColor = label.labelColor || getGlobalConfig(globalLabelsConfig, 'labelColor', '#000');
    model.labelRollOverColor = label.labelRollOverColor || getGlobalConfig(globalLabelsConfig, 'labelRollOverColor', '#000');
}

function getGlobalConfig(config, property, defaultValue){
  if (!config) return defaultValue;
  if (!config[property]) return defaultValue;
  return config[property];
}

function setUrlConfig(model, url){
    model.url = libs.portal.pageUrl({id:url.url});
}

function setColorConfig(model, color){
    model.color = color.color;
}

function setNotificationConfig(model, notification){
    if (!notification)return;
    model.notification = libs.portal.processHtml({value:notification.notification});
    if (notification.notyConfig){
      model.notyConfig = notification.notyConfig;
      if (notification.notyConfig.buttons){
        var buttons = forceArray(notification.notyConfig.buttons);
        model.notyConfig.buttons = [];
        buttons.forEach(function(button){
          var buttonModel = {
            title: button.title,
            class: button.class || 'countryswitcherButton'
          };
          if (button.url){
            buttonModel.url = libs.portal.pageUrl({id:button.url});
          }
          model.notyConfig.buttons.push(buttonModel);
        });
      }
    }
}

function getAmChartModelBase(){
  var siteConfig = libs.portal.getSiteConfig();
  var baseConfig = {
    type: "map",
    projection:"miller",

    responsive: {
        enabled: true,
        addDefaultRules: true,
        rules: [
          {
            maxWidth: 600,
            overrides:{
              projection:"winkel3"
            }
          }
        ]
    },

    imagesSettings: {
      rollOverColor: "#CC0000",
      rollOverScale: 1,
      selectedScale: 1,
      selectedColor: "#CC0000"
    },

    zoomControl: {
      zoomControlEnabled: true
    },

    areasSettings: {
      outlineThickness:0.5,
      selectable: true,
      autoZoom: true,
      selectedColor: '#CC0000',
      color: siteConfig.areasSettings.color || '#aaa'
    },

    objectList: {
      container: "listdiv"
    },
    showImagesInList: true

  };
  return baseConfig;
}


function forceArray(data) {
    if (!Array.isArray(data)) {
        data = [data];
    }
    return data;
};

exports.forceArray = forceArray;
