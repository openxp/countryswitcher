var libs = {
  thymeleaf: require('/lib/xp/thymeleaf'),
  portal: require('/lib/xp/portal'),
  countryswitcher: require('/lib/openxp/countryswitcher')
}

exports.get = function(req){
    var model = {countries:[]};
    var component = libs.portal.getComponent();
    var config = component.config;
    var view = resolve('flags-with-link.html');
    var countries = libs.countryswitcher.forceArray(config.countries);
    if (countries){
      countries.forEach(function(country){
        if (country){
          var countryModel = {
            country: country.country,
            url: libs.portal.pageUrl({id: country.url})
          }
          model.countries.push(countryModel);
        }
      });
    }
    var pageContributions = {headEnd:[]};
    pageContributions.headEnd.push('<link rel="stylesheet" href="'+
    libs.portal.assetUrl({path: 'flags-sprites/flags.min.css'})+'"></link>');
    pageContributions.headEnd.push('<link rel="stylesheet" href="'+
    libs.portal.assetUrl({path: 'countryswitcher.css'})+'"></link>');

    return {
      body: libs.thymeleaf.render(view, model),
      pageContributions:pageContributions
    }
}
