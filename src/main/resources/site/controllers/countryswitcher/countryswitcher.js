var libs = {
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    content: require('/lib/xp/content'),
    countryswitcher: require('/lib/openxp/countryswitcher')
};

exports.get = handleGet;

function handleGet(req) {
    var content = libs.portal.getContent();
    var siteConfig = libs.portal.getSiteConfig();
    delete siteConfig.notyConfig.buttons;
    var model = {
      globalNotyConfig: siteConfig.notyConfig
    };
    model.amChartModel = libs.countryswitcher.getAmChartModel(content);
    if (content.data.config && content.data.config.clickOutsideAreasNotification){
      var clickOutsideAreasModel = {}
      libs.countryswitcher.getNotificationConfig(clickOutsideAreasModel, content.data.config.clickOutsideAreasNotification);
      model.amChartModel.clickOutsideAreas = clickOutsideAreasModel;
      log.info(JSON.stringify(model.amChartModel));
    }
    if (model !== undefined){
      model.mode = req.mode;
    }
    return {
        body: libs.thymeleaf.render(resolve('countryswitcher.html'), model)
    };
}
